; -*- system :ДОК-НЯ; coding: utf-8; -*- 

(named-readtables::in-readtable :buddens-readtable-a)
(in-package :ДОК-НЯ)

;; ПРАВЬМЯ: забрать остальные документы из c:\ob\novyjj-komponentnyjj-paskalq.t\Dev\Docu\ru

(СЕКЦИЯ СЕКЦИЯ-ДОК-НЯ ("" () "")

  (СЕКЦИЯ СЕКЦИЯ-Общее ("" () "")

          (! состав-компилятора "" () "
Состав компилятора
Важная информация о структурах данных компилятора - в конце модуля НяФс.

```
НяЛексер - лексер, существует в единственном экземпляре
НяПарсер - парсер, то же
НяМ - Вычитывание_исходного_текста_модуля_из_отображения, 
      Чтение_простых_типов_из_символьного_файла
      Запись_простых_типов_в_символьный_файл
      Запись_объектного_файла
НяФк - порождение кодового файла
НяД - строит синтаксическое дерево, проводит контроль и неявное преобразование типов
НяФс - файлы символов: создание, импорт, поиск
НяГенКода486 - генератор кода
 НяИспользование - UseCalls и UseReals, что делают - не знаю.
 НяГ486Ву - генератор кода, высокий уровень (?)
  НяГ486Ну - генератор кода, низкий уровень (?)
```

См. также [http://obertone.ru/_media/bb/op2.paper.pdf](http://obertone.ru/_media/bb/op2.paper.pdf)  : 

См. также [http://obertone.ru/blackbox/dev](http://obertone.ru/blackbox/dev)
                                       ")

          (! опции-компиляции "" () "
Опции компилятора ищутся в документации ББ по словам «Особенности, зависящие от платформы». 
                                     ")
         ) ; СЕКЦИЯ-Общее

;; ПРАВЬМЯ: забрать остальные документы из c:\ob\novyjj-komponentnyjj-paskalq.t\Dev\Docu\ru

  (СЕКЦИЯ СЕКЦИЯ-описания-отдельных-файлов ("" () "")
          (! ня-компилятор "" () "
DevCompiler

Пакет команд для компилятора Ня. 
Критичные для безопасности проверки времени выполнения проводятся всегда (охрана типов, проверки границ массивов, и т.п.), в то время как 
не критичные проверки не могут генерироваться вообще (SHORT, переполнение целых, проверка вхождения во множества). 
«Критичные» означает, что может быть повреждена нелокальная память, с неизвестными глобальными эффектами.

PROCEDURE Compile - Компилирует модуль Ня, исходный текст которого находится в фокусированном отображении.

PROCEDURE CompileAndUnload - компилирует модуль, исходный текст которого находится в фокусированном отображении. Если компиляция проходит успешно, то старая версия этого модуля выгружается. CompileAndUnload полезна при разработке модулей верхнего уровня, то есть таких, которые не импортируются другими модулями и поэтому могут быть отдельно выгружены.

PROCEDURE CompileModuleList - компилирует список модулей, имена которых выделены. Когда обнаруживается первая ошибка, содержащий ее исходный код открывается, показывая ошибку.

PROCEDURE CompileSelection - компилирует модуль, начало которого выделено.

PROCEDURE CompileThis - используется в тексте с DevCommanders.View. Эта команда берет текст, следующий за ним, и интерпретирует его как список модулей, которые следует откомпилировать. Похожа на CompileModuleList за исключением того, что не нужно выделение.
" :СМ-ТАКЖЕ (опции-компиляции))

          (! ня-парсер "" () "

Парсер. Проще всего идти по грамматике из определения языка. Каждый элемент грамматики переведён на русский. 
Функция разбора этого элемента называется «Разбери<ЭтотЭлемент>», например, «РазбериМодуль», но может содержать 
и уточнения. Отфильтровать все можно поиском по подстроке «PROCEDURE Разбери»")

          ) ; СЕКЦИЯ СЕКЦИЯ-ОПИСАНИЕ-ОТДЕЛЬНЫХ-ФАЙЛОВ

  (СЕКЦИЯ СЕКЦИЯ-процесс-документирования ("" () "")
          (! обращение-с-константами "" (ПРОЦЕСС-ДОКУМЕНТИРОВАНИЯ) "
У нас нет инструментов рефакторинга, чтобы исключить дублирование блоков констант (а можно было бы их положить в отдельный модуль). 
Нужно реанимировать ПроверитьИнклюды и применить здесь, чтобы проверять их совпадение. Пока что каждая группа констант описывается в 
одноимённой статье данного документа с тегом ОПИСАНИЕ-ГРУППЫ-КОНСТАНТ . Если понимаем, переводим. Если нет - оставляем.
При переводе не убираем комментарии, а добавляем к ним русские рядом - 
на случай, если мы пропустим часть английских, чтобы у нас оставалась 
смысловая связка.
")

          (! правила-переименования "" (ПРОЦЕСС-ДОКУМЕНТИРОВАНИЯ) "
EnglishName => РусскоеИмя_EnglishName предпочтительно
Но иногда можно просто превратить в русское имя. Тогда английское надо упомянуть хотя бы в комментарии, в угловых скобках. 
")
          (! словарь "" (ПРОЦЕСС-ДОКУМЕНТИРОВАНИЯ) "
Лексер выдаёт лексемы, а не символы.

Assert = Убдеись

New = Яви (т.к. этот New вызывает тот, к кому мы сейчас обращаемся. Явись было бы, если бы обращались к самому объекту, который должен родиться)

Record = Структа

Процедура внутри другой процедуры - Подпроцедура


[Кл.слова из школьной сборки](http://вече.программирование-по-русски.рф/viewtopic.php?f=2&t=23)
")

          (! правила-документирования-определений "" (ПРОЦЕСС-ДОКУМЕНТИРОВАНИЯ) "
```
MODULE ZZZ;
(**** Комментарий к модулю *)

(** Полный комментарий *)
Name = RECORD (* Краткий комментарий *)
  (** Подробный комментарий к полю,
          если надо *)
  Field (* Здесь комментарий *)


(** Полный комментарий *)
PROCEDURE Имя(args); (* <OldName> Краткий комментарий *)

(* ? Информация в комментарии и имя объекта требуют уточнения *)
```")

          (! правила-документирования-разделов "" (ПРОЦЕСС-ДОКУМЕНТИРОВАНИЯ) "
```
 (*** Название_раздела , см. также Название_другого_раздела *)
```
")                                                                             

          
          ) ; СЕКЦИЯ-ПРОЦЕСС-ДОКУМЕНТИРОВАНИЯ

  (СЕКЦИЯ СЕКЦИЯ-понятия ("" () "")
          (! пример-SYSTEM-точка-VAL "" ()
    """Пытаемся взять одну функцию SYSTEM.VAL и отследить
её историю по всему компилятору. 

### Регистрация

Функция регистрируется (становится известной компилятору) 
в модуле НямФС: `EnterProc("VAL", valfn);`

Далее нужно искать valfn. 

### Документация

«Особенности, зависящие от платформы», Dev/Docu/P-S-I.odc
```
VAL(T, x), значение значение x
интерпретируется как имеющее тип T
```

### Участие в исходных текстах

НяД.StPar0 - обработка первого параметра стандартной функции. 
Обработки никакой не происходит, дело сводится к проверкам.

НяД.StPar1 - обработка второго параметра стандартной ф-ии (он обозначается x).
Главное здесь - в конце. Тип результата (x.typ) назначается равным p.typ (типу первого
параметра), а значение результата берётся из второго параметра (p := x)
В итоге первый аргумент и сам вызов SYSTEM.VAL удаляются из дерева. 

### Итого
В простом случае обработка функции VAL сводится к тому, что её вызов выбрасывается, 
узел её второго аргумента получает другой тип времени компиляции, и его значение
возвращается.
""")

   (! о-параметрах-процедур "" (СОСТОЯНИЕ-КОМПИЛЯТОРА СИМВОЛЬНАЯ-ИНФОРМАЦИЯ) "Цель - узнать, как вставить 
в код (funcall ,(intern имя модуль) ... ), где имя и модуль - константы, известные 
во время компиляции.
 
 НяПарсер.GetParams
   НяПарсер.FormalParameters(proc.link, proc.typ, name)
   НяД.CheckParameters                             

")


   (! таблицы-символов "" (СОСТОЯНИЕ-КОМПИЛЯТОРА СИМВОЛЬНАЯ-ИНФОРМАЦИЯ) 
"
НяФс.Import - импортирует модуль (объявление импорта одного файла в директиве IMPORT)
Ищи комментарий «Импорт_модуля» . Общая идея: открывает символьный файл, всё читает из 
него, и вешает в виде графа связей в GlbMod. 

Главный тип таблицы символов - это, похоже, НяФс.ObjDesc и указатель на него НяФс.Object
")
          ) ; СЕКЦИЯ-понятия

(СЕКЦИЯ СЕКЦИЯ-Описания-групп-констант ("" () "")
          (! numtyp-values "" (ОПИСАНИЕ-ГРУППЫ-КОНСТАНТ) "char = 1; integer = 2; real = 4; int64 = 5; real32 = 6; real64 = 7;
")

          (! symbol-values-1-16 "symbol values 1-16" (ОПИСАНИЕ-ГРУППЫ-КОНСТАНТ)
"Символы арифметических операций, включая сравнения, 
логических операций, а также is и in")

          (! symbol-values-17-76 "symbol values 17-76" (ОПИСАНИЕ-ГРУППЫ-КОНСТАНТ)
"Символы разделителей, ключевые слова языка" 
:СМ-ТАКЖЕ (symbol-values-20-76))

          (! symbol-values-20-76 "symbol values 20-76" (ОПИСАНИЕ-ГРУППЫ-КОНСТАНТ)
"Похоже на подмножество symbol-values-17-76, но, как минимум, 
moudle (moduleSym) отличается написанием")

          (! object-modes "object modes" (ОПИСАНИЕ-ГРУППЫ-КОНСТАНТ)
"Может находиться в поле ObjDesc.mode. Например, символьная информация модуля имеет
ObjDesc.mode = 11 (Mod)")

          (! классы-типов-данных "Классы типов данных (structure types)" (ОПИСАНИЕ-ГРУППЫ-КОНСТАНТ)
"Structure types Грубая классификация типов данных 
 Перекликаются с тегами типов (см. Kernel DevHeapSpy.FormOf),
 но не полностью совпадают.")
            ) ; СЕКЦИЯ-Описания-групп-констант


  (СЕКЦИЯ СЕКЦИЯ-справочные-таблицы ("" () "")
          (! таблица-описания-объектов "" (СПРАВОЧНАЯ-ТАБЛИЦА) "
Выдернуто из (Ня)Фс.kp

Objects:
```
    mode  | adr    conval  link     scope    leaf
  ------------------------------------------------
    Undef |                                         Not used
    Var   | vadr           next              regopt Glob or loc var or proc value parameter
    VarPar| vadr           next              regopt Var parameter (vis = 0 | inPar | outPar)
    Con   |        val                              Constant
    Fld   | off            next                     Record field
    Typ   |                                         Named type
    LProc | entry  sizes   firstpar scope    leaf   Local procedure, entry adr set in back-end
    XProc | entry  sizes   firstpar scope    leaf   External procedure, entry adr set in back-end
    SProc | fno    sizes                            Standard procedure
    CProc |        code    firstpar scope           Code procedure
    IProc | entry  sizes            scope    leaf   Interrupt procedure, entry adr set in back-end
    Mod   |                         scope           Module
    Head  | txtpos         owner    firstvar        Scope anchor
    TProc | entry  sizes   firstpar scope    leaf   Bound procedure, mthno = obj.num
```
          ")

          (! таблица-описания-структур "" (СПРАВОЧНАЯ-ТАБЛИЦА) "
Выдернуто из (Ня)Фс.kp
                                                    
  Structures:
```
    form    comp  | n      BaseTyp   link     mno  txtpos   sysflag
----------------------------------------------------------------------------------
    Undef   Basic |
    Byte    Basic |
    Bool    Basic |
    Char8   Basic |
    Int8    Basic |
    Int16   Basic |
    Int32   Basic |
    Real32  Basic |
    Real64  Basic |
    Set     Basic |
    String8 Basic |
    NilTyp  Basic |
    NoTyp   Basic |
    Pointer Basic |        PBaseTyp           mno  txtpos   sysflag
    ProcTyp Basic |        ResTyp    params   mno  txtpos   sysflag
    Comp    Array | nofel  ElemTyp            mno  txtpos   sysflag
    Comp    DynArr| dim    ElemTyp            mno  txtpos   sysflag
    Comp    Record| nofmth RBaseTyp  fields   mno  txtpos   sysflag
    Char16  Basic |
    String16Basic |
    Int64   Basic |
```

        ")
          (! таблица-описания-узлов "" (СПРАВОЧНАЯ-ТАБЛИЦА) "
Выдернуто из (Ня)Фс.kp

Здесь Ntype - видимо, литерал, указывающий тип, например, CHAR. 
Ncall - вызов процедуры.

Nodes:
```
design   = Nvar|Nvarpar|Nfield|Nderef|Nindex|Nguard|Neguard|Ntype|Nproc.
expr     = design|Nconst|Nupto|Nmop|Ndop|Ncall.
nextexpr = NIL|expr.
ifstat   = NIL|Nif.
casestat = Ncaselse.
sglcase  = NIL|Ncasedo.
stat     = NIL|Ninittd|Nenter|Nassign|Ncall|Nifelse|Ncase|Nwhile|Nrepeat|
          Nloop|Nexit|Nreturn|Nwith|Ntrap.


              class     subcl     obj      left      right     link      
              ---------------------------------------------------------

design        Nvar                var                          nextexpr
              Nvarpar             varpar                       nextexpr
              Nfield              field    design              nextexpr
              Nderef    ptr/str            design              nextexpr
              Nindex                       design    expr      nextexpr
              Nguard                       design              nextexpr (typ = guard type)
              Neguard                      design              nextexpr (typ = guard type)
              Ntype               type                         nextexpr
              Nproc     normal    proc                         nextexpr
                        super     proc                         nextexpr


expr          design
              Nconst              const                                 (val = node.conval)
              Nupto                        expr      expr      nextexpr 
              Nmop      not                expr                nextexpr
                        minus              expr                nextexpr
                        is        tsttype  expr                nextexpr
                        conv               expr                nextexpr
                        abs                expr                nextexpr
                        cap                expr                nextexpr
                        odd                expr                nextexpr
                        bit                expr                nextexpr {x}
                        adr                expr                nextexpr SYSTEM.ADR
                        typ                expr                nextexpr SYSTEM.TYP
                        cc                 Nconst              nextexpr SYSTEM.CC
                        val                expr                nextexpr SYSTEM.VAL
              Ndop      times              expr      expr      nextexpr
                        slash              expr      expr      nextexpr
                        div                expr      expr      nextexpr
                        mod                expr      expr      nextexpr
                        and                expr      expr      nextexpr
                        plus               expr      expr      nextexpr
                        minus              expr      expr      nextexpr
                        or                 expr      expr      nextexpr
                        eql                expr      expr      nextexpr
                        neq                expr      expr      nextexpr
                        lss                expr      expr      nextexpr
                        leq                expr      expr      nextexpr
                        grt                expr      expr      nextexpr
                        geq                expr      expr      nextexpr
                        in                 expr      expr      nextexpr
                        ash                expr      expr      nextexpr
                        msk                expr      Nconst    nextexpr
                        len                design    Nconst    nextexpr
                        min                expr      expr      nextexpr MIN
                        max                expr      expr      nextexpr MAX
                        bit                expr      expr      nextexpr SYSTEM.BIT
                        lsh                expr      expr      nextexpr SYSTEM.LSH
                        rot                expr      expr      nextexpr SYSTEM.ROT
              Ncall               fpar     design    nextexpr  nextexpr
              Ncomp                        stat      expr      nextexpr

nextexpr      NIL
              expr

ifstat        NIL
              Nif                          expr      stat      ifstat

casestat      Ncaselse                     sglcase   stat           (minmax = node.conval)

sglcase       NIL
              Ncasedo                      Nconst    stat      sglcase

stat          NIL
              Ninittd                                          stat     (of node.typ)
              Nenter              proc     stat      stat      stat     (proc=NIL for mod)
              Nassign   assign             design    expr      stat
                        newfn              design    nextexp   stat
                        incfn              design    expr      stat
                        decfn              design    expr      stat
                        inclfn             design    expr      stat
                        exclfn             design    expr      stat
                        copyfn             design    expr      stat
                        getfn              design    expr      stat     SYSTEM.GET
                        putfn              expr      expr      stat     SYSTEM.PUT
                        getrfn             design    Nconst    stat     SYSTEM.GETREG
                        putrfn             Nconst    expr      stat     SYSTEM.PUTREG
                        sysnewfn           design    expr      stat     SYSTEM.NEW
                        movefn             expr      expr      stat     SYSTEM.MOVE
                                                                        (right.link = 3rd par)
              Ncall               fpar     design    nextexpr  stat
              Nifelse                      ifstat    stat      stat
              Ncase                        expr      casestat  stat
              Nwhile                       expr      stat      stat
              Nrepeat                      stat      expr      stat
              Nloop                        stat                stat 
              Nexit                                            stat 
              Nreturn             proc     nextexpr            stat     (proc = NIL for mod)
              Nwith                        ifstat    stat      stat
              Ntrap                                  expr      stat
              Ncomp                        stat      stat      stat
```
            ")


          )  ; СЕКЦИЯ-Справочные-таблицы


) ; СЕКЦИЯ
